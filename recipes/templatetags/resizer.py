from django import template
from recipes.models import Recipe, Ingredient

register = template.Library()


def resize_to(ingredient, target):
    num_servings = ingredient.recipe.servings
    amount = ingredient.amount
    if num_servings is not None and target is not None:
        try:
            print(num_servings)
            print(amount)
            ratio = int(target) / int(num_servings)
            new_amount = ratio * int(amount)
            return str(new_amount)
        except:
            pass
    return amount


register.filter(resize_to)
