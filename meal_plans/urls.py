from django.urls import path
from meal_plans.views import (
    Meal_plansDetailView,
    Meal_plansListView,
    Meal_plansCreateView,
    Meal_plansUpdateView,
    Meal_plansDeleteView,
)

urlpatterns = [
    path("", Meal_plansListView.as_view(), name="meal_plans_list"),
    path("create/", Meal_plansCreateView.as_view(), name="meal_plans_create"),
    path("<int:pk>/", Meal_plansDetailView.as_view(), name="meal_plans_detail"),
    path(
        "<int:pk>/edit/", Meal_plansUpdateView.as_view(), name="meal_plans_edit"
    ),
    path(
        "<int:pk>/delete/",
        Meal_plansDeleteView.as_view(),
        name="meal_plans_delete",
    ),
]
