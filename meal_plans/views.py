from django.shortcuts import redirect, render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from meal_plans.models import MealPlan
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from recipes.models import Recipe


class Meal_plansListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 4


    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class Meal_plansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/create.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plans_detail", args=[self.object.id])


class Meal_plansDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"
    context_object_name = "meal_plan"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class Meal_plansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plans_detail", args=[self.object.id])


class Meal_plansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
